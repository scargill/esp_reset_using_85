A simple reset circuit for use with FTDI, Eclipse and an ESP-12 for example. using an ATTINY85

The tiny waits a fraction of a second and from there on for 10 seconds listens for serial input.
If it gets none it goes into a permanent loop. If it sees serial input on input 2 it will reset the ESP,
pull down GPIO0 and then release both putting the ESP into programming mode, it will then wait until 
programming is finished, tristate it's pins and go into a permanent loop.

See Aidan's diagram.